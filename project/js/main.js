(function() {
  var closeAdaptiveMenue, closeModalWindows, closeSandwich, getBodyScrollTop, openModalWindow, openSandwich, resizeCells, resizeModalWindows, resizeSandwich, scrollCurrent, scrollTo;

  window.currentScroll = 0;

  $(document).ready(function() {
    var days, month;
    $(".cat-slider .slider .item").click(function() {
      var big, bigSlider, title;
      big = $(this).attr("data-big");
      title = $(this).attr("data-title");
      bigSlider = $(".cat-slider .big-img");
      bigSlider.css({
        "background-image": "url(" + big + ")"
      });
      return bigSlider.find("div").html(title);
    });
    $(".product-slider .slider .item").click(function() {
      var big, bigSlider, title;
      big = $(this).attr("data-big");
      title = $(this).attr("data-title");
      bigSlider = $(".product-slider .big-img");
      bigSlider.css({
        "background-image": "url(" + big + ")"
      });
      return bigSlider.find("div").html(title);
    });
    $(".switcher").click(function() {
      return $(this).toggleClass("off");
    });
    $(".two-columns .catalog .head").click(function() {
      var wWidth;
      wWidth = $(window).width();
      if (wWidth < 1000) {
        return $(this).closest(".catalog").find("ul").slideToggle(333);
      }
    });
    $(".sandwich-menue ul li a").click(function() {
      var count;
      count = 0;
      $(this).closest("li").find("ul").each(function() {
        return count += 1;
      });
      if (count !== 0) {
        $(this).closest("li").find("ul").slideToggle(333);
        $(this).closest("li").toggleClass("active");
        return false;
      }
    });
    $(".fancybox").fancybox();
    $(".sandwich").click(function() {
      if ($(this).hasClass("open")) {
        $(this).removeClass("open");
        return closeSandwich();
      } else {
        $(this).addClass("open");
        return openSandwich();
      }
    });
    $(".show-cut").click(function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this).closest(".container").find(".cut").slideUp(333);
        return $(this).html("Подробнее");
      } else {
        $(this).addClass("active");
        $(this).closest(".container").find(".cut").slideDown(333);
        return $(this).html("Скрыть");
      }
    });
    month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
    days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
    $(".submit-this").click(function() {
      return $(this).closest("form").submit();
    });
    $('input').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red',
      increaseArea: '20%'
    });
    $(".index-slider").owlCarousel({
      navigation: true,
      pagination: false,
      slideSpeed: 444,
      paginationSpeed: 333,
      singleItem: true,
      touchDrag: true,
      autoPlay: 5000,
      navigationText: false
    });
    $(".gallery-slider").owlCarousel({
      navigation: true,
      pagination: false,
      slideSpeed: 444,
      paginationSpeed: 333,
      singleItem: false,
      items: 4,
      touchDrag: true,
      autoPlay: false,
      navigationText: false
    });
    $(".cat-slider .slider").owlCarousel({
      navigation: true,
      pagination: false,
      slideSpeed: 444,
      paginationSpeed: 333,
      singleItem: false,
      items: 8,
      touchDrag: true,
      autoPlay: false,
      navigationText: false
    });
    $(".product-page .gallery .slider").owlCarousel({
      navigation: true,
      pagination: false,
      slideSpeed: 444,
      paginationSpeed: 333,
      singleItem: false,
      items: 4,
      touchDrag: true,
      autoPlay: false,
      navigationText: false
    });
    resizeModalWindows();
    resizeSandwich();
    $(".get-zamer").click(function() {
      openModalWindow("#zamer-form-place");
      return false;
    });
    $(".get-callback").click(function() {
      openModalWindow("#callback-form-place");
      return false;
    });
    $(".modal-window .icons-close, .darkness, .sandwich-menue .title").click(function() {
      closeModalWindows();
      return closeSandwich();
    });
    $(".menue-list .title").click(function() {
      $(this).closest(".section").find(".row").slideToggle(108);
      return $(this).toggleClass("active");
    });
    $(".open-adaptive-menue").click(function() {});
    return $(".open-hidden-buttons").click(function() {
      closeAdaptiveMenue();
      $(".hidden-buttons-menue").show();
      return $(".darkness").show();
    });
  });

  $(window).resize(function() {
    resizeModalWindows();
    return resizeSandwich();
  });

  $(window).scroll(function() {
    if ($("body").hasClass("open-modal-window")) {

    } else {
      return window.currentScroll = getBodyScrollTop();
    }
  });

  openModalWindow = function(xx) {
    closeAdaptiveMenue();
    xx = $(xx);
    $("body").addClass("open-modal-window");
    $(".modal-window").hide();
    xx.show();
    xx.css({
      "position": "absolute"
    });
    $(".wrapper").css({
      "position": "fixed",
      "top": currentScroll * -1
    });
    return $(".darkness").show();
  };

  closeModalWindows = function() {
    var xx;
    xx = $(".modal-window");
    xx.hide();
    xx.css({
      "position": "absolute"
    });
    $(".wrapper").css({
      "position": "relative",
      "top": 0
    });
    $(".darkness").hide();
    closeAdaptiveMenue();
    scrollCurrent();
    return $("body").removeClass("open-modal-window");
  };

  closeAdaptiveMenue = function() {
    return $(".adaptive-menue").hide();
  };

  resizeModalWindows = function() {
    var wHeight;
    wHeight = $(window).height() / 10;
    return $(".modal-window .place").css({
      "marginTop": wHeight
    });
  };

  resizeSandwich = function() {
    var denominator, wHeight, wWidth;
    wWidth = $(window).width();
    wHeight = $(window).height();
    denominator = 3;
    if (wWidth < 800) {
      denominator = 2;
    }
    if (wWidth < 700) {
      denominator = 1.5;
    }
    if (wWidth < 600) {
      denominator = 1.25;
    }
    return $(".sandwich-menue").css({
      "height": wHeight - 48
    });
  };

  openSandwich = function() {
    return $(".sandwich-menue").addClass("open");
  };

  closeSandwich = function() {
    return $(".sandwich-menue").removeClass("open");
  };

  resizeCells = function(object) {
    var blocksArray, blocksMax;
    if (object === void 0) {
      object = $(".cell-need-resize .cell");
    }
    blocksArray = [];
    object.each(function() {
      var blockHeight;
      blockHeight = $(this).height();
      return blocksArray.push(blockHeight);
    });
    blocksMax = Math.max.apply(null, blocksArray);
    return object.css({
      "height": blocksMax
    });
  };

  getBodyScrollTop = function() {
    return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop);
  };

  scrollCurrent = function() {
    return $('html, body').animate({
      scrollTop: currentScroll
    }, 0);
  };

  scrollTo = function(target) {
    var scrollPath;
    scrollPath = $(target).offset().top - 40;
    return $('html, body').animate({
      scrollTop: scrollPath
    }, 333);
  };

}).call(this);

