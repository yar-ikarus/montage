(function() {
  var sendForm;

  $(document).ready(function() {
    $("#index-call-form .btn").click(function() {
      return sendForm("#index-call-form");
    });
    $("#index-order-form .btn").click(function() {
      return sendForm("#index-order-form");
    });
    $("#top-callback-form .btn").click(function() {
      return sendForm("#top-callback-form");
    });
    $("#zamer-callback-form .btn").click(function() {
      return sendForm("#zamer-callback-form");
    });
    $("#main-request-form .btn").click(function() {
      return sendForm("#main-request-form");
    });
    $("#modal-callback-form .btn").click(function() {
      return sendForm("#modal-callback-form");
    });
    $("#index-phone-form .btn").click(function() {
      return sendForm("#index-phone-form");
    });
    $("#big-form .btn").click(function() {
      return sendForm("#big-form");
    });
    $("#big-red-form .btn").click(function() {
      return sendForm("#big-red-form");
    });
    $("#express-application-1 .btn").click(function() {
      return sendForm("#express-application-1");
    });
    $("#express-application-2 .btn").click(function() {
      return sendForm("#express-application-2");
    });
    $("#express-application-3 .btn").click(function() {
      return sendForm("#express-application-3");
    });
    $("#express-application-4 .btn").click(function() {
      return sendForm("#express-application-4");
    });
    return $("#footer-form .btn").click(function() {
      return sendForm("#footer-form");
    });
  });

  sendForm = function(form) {
    form = $(form);
    form.validate();
    if (form.valid()) {
      form.find('.btn').attr('disabled', 'disabled');
      form.slideUp().parent().append('<h4 class="text-center">В ближайшее время наш менеджер свяжется с вами.</h4>');
      return $.post("/forms.php", form.serialize()).done(function(data) {
        return {};
      });
    }
  };

}).call(this);

