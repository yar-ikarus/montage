window.currentScroll = 0

$(document).ready ->

	$(".cat-slider .slider .item").click ->
		big = $(this).attr("data-big")
		title = $(this).attr("data-title")

		bigSlider = $(".cat-slider .big-img")
		
		bigSlider.css 
			"background-image": "url("+big+")"
		bigSlider.find("div").html title

	$(".product-slider .slider .item").click ->
		big = $(this).attr("data-big")
		title = $(this).attr("data-title")

		bigSlider = $(".product-slider .big-img")
		
		bigSlider.css 
			"background-image": "url("+big+")"
		bigSlider.find("div").html title

	$(".switcher").click ->
		$(this).toggleClass "off"


	$(".two-columns .catalog .head").click ->
		wWidth = $(window).width()
		if wWidth < 1000
			$(this).closest(".catalog").find("ul").slideToggle(333)


	$(".sandwich-menue ul li a").click ->
		count = 0
		$(this).closest("li").find("ul").each ->	
			count += 1
		if count != 0
			$(this).closest("li").find("ul").slideToggle(333)
			$(this).closest("li").toggleClass "active"
			return false


	$(".fancybox").fancybox()

	$(".sandwich").click ->
		if $(this).hasClass "open"
			$(this).removeClass "open"
			closeSandwich()
		else 
			$(this).addClass "open"
			openSandwich()

	$(".show-cut").click ->
		if $(this).hasClass "active"
			$(this).removeClass "active"
			$(this).closest(".container").find(".cut").slideUp(333)
			$(this).html "Подробнее"
		else
			$(this).addClass "active"
			$(this).closest(".container").find(".cut").slideDown(333)
			$(this).html "Скрыть"

	month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
	days = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']

	
	$(".submit-this").click ->
		$(this).closest("form").submit()

	$('input').iCheck
		checkboxClass: 'icheckbox_minimal-red'
		radioClass: 'iradio_minimal-red'
		increaseArea: '20%'

	$(".index-slider").owlCarousel
		navigation: yes
		pagination: no
		slideSpeed: 444
		paginationSpeed: 333
		singleItem: yes
		touchDrag: yes
		autoPlay: 5000
		navigationText: no

	$(".gallery-slider").owlCarousel
		navigation: yes
		pagination: no
		slideSpeed: 444
		paginationSpeed: 333
		singleItem: no
		items: 4
		touchDrag: yes
		autoPlay: no
		navigationText: no

	$(".cat-slider .slider").owlCarousel
		navigation: yes
		pagination: no
		slideSpeed: 444
		paginationSpeed: 333
		singleItem: no
		items: 8
		touchDrag: yes
		autoPlay: no
		navigationText: no

	$(".product-page .gallery .slider").owlCarousel
		navigation: yes
		pagination: no
		slideSpeed: 444
		paginationSpeed: 333
		singleItem: no
		items: 4
		touchDrag: yes
		autoPlay: no
		navigationText: no

	resizeModalWindows()
	resizeSandwich()

	$(".get-zamer").click ->
		openModalWindow("#zamer-form-place")
		return false

	$(".get-callback").click ->
		openModalWindow("#callback-form-place")
		return false
	
	$(".modal-window .icons-close, .darkness, .sandwich-menue .title").click ->
		closeModalWindows()
		closeSandwich()

	# /menue/
	$(".menue-list .title").click ->		
		$(this).closest(".section").find(".row").slideToggle(108)
		$(this).toggleClass("active")

	# adaptive menue
	$(".open-adaptive-menue").click ->
		#openSandwich()		

	$(".open-hidden-buttons").click ->
		closeAdaptiveMenue()
		$(".hidden-buttons-menue").show()
		$(".darkness").show()

$(window).resize ->
	resizeModalWindows()
	resizeSandwich()

$(window).scroll ->
	if $("body").hasClass("open-modal-window")
		#
	else 
		window.currentScroll = getBodyScrollTop()

openModalWindow = (xx) ->
	closeAdaptiveMenue()
	xx = $(xx)
	$("body").addClass("open-modal-window")
	$(".modal-window").hide()
	xx.show()
	xx.css
		"position": "absolute"
	$(".wrapper").css
		"position": "fixed"
		"top": currentScroll * -1
	$(".darkness").show()

closeModalWindows = ->
	xx = $(".modal-window")
	xx.hide()
	xx.css
		"position": "absolute"
	$(".wrapper").css
		"position": "relative"
		"top": 0
	$(".darkness").hide()
	closeAdaptiveMenue()
	scrollCurrent()
	$("body").removeClass("open-modal-window")

closeAdaptiveMenue = ->
	$(".adaptive-menue").hide()

resizeModalWindows = ->
	wHeight = $(window).height() / 10
	$(".modal-window .place").css
		"marginTop": wHeight

resizeSandwich = ->
	
	wWidth = $(window).width()
	wHeight = $(window).height()
	denominator = 3

	if wWidth < 800 
		denominator = 2
	if wWidth < 700
		denominator = 1.5	
	if wWidth < 600
		denominator = 1.25

	#$(".sandwich-menue").width(wWidth / denominator)
	$(".sandwich-menue").css
		"height":wHeight - 48
	

openSandwich = ->
	#$(".darkness").show()
	$(".sandwich-menue").addClass "open"
closeSandwich = ->
	#$(".sandwich").addClass "open"
	$(".sandwich-menue").removeClass "open"

resizeCells = (object) ->
	#функция которая делает одинаковой высоту во всех блоках .cell
	if object == undefined
		object = $(".cell-need-resize .cell")
	blocksArray = []
	object.each ->
		blockHeight = $(this).height()
		blocksArray.push blockHeight
	#а теперь узнаем самое большое значение в массиве
	blocksMax = Math.max.apply null, blocksArray
	#это и есть высота которая будет задана всем блокам
	object.css 
		"height": blocksMax

getBodyScrollTop =  ->
	return self.pageYOffset || (document.documentElement && document.documentElement.scrollTop) || (document.body && document.body.scrollTop)

scrollCurrent = () ->
	$('html, body').animate
		scrollTop: currentScroll
	,0
scrollTo = (target) ->
	scrollPath = $(target).offset().top - 40
	$('html, body').animate
		scrollTop: scrollPath
	,333