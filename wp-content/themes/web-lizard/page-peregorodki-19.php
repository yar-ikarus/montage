<?php
/**
 * Template Name: 19 Глухие двери в офис в Санкт-Петербурге
 * @package WordPress
 * @subpackage your-clean-template
 */

			
get_header(); // подключаем header.php ?>
<? include 'inc/index-slider.php';?>
<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
<div class="page-default page-article page-peregorodki-2">
	<div class="container">
		<div class="place">
			<h1><?php the_title(); ?></h1>
			<div class="group">
				<div class="cell size-50">
					<img src="/project/images/other/articles/42.jpg">
				</div>
				<div class="cell size-50">
					<p>Сегодня глухие двери считаются идеальным вариантом для установки между комнатами. Обладая хорошими звукоизоляционными свойствами, они надежно защитят от стороннего шума. Исходя из названия конструкций, можно понять, что двери глухие представляют собой сплошное полотно, не требующее остекления.  К тому же их изготовляют из разных материалов: красного дерева (меранти), ореха, дуба. </p>
					<p>Главное достоинство глухих дверей заключается в том, что они могут прослужить не один десяток лет, не потеряв при этом изначального внешнего вида. Они не нуждаются в каком-либо особом уходе. </p>
					<p>Межкомнатные двери с поверхностью из филенок составлены из полотна, на котором с 2 сторон есть округлые и прямоугольные углубления. Такие изделия бывают облегченными, полумассивными и массивными. Филенки у них оклеивают фанерой, толщиной от 1 до 5 мм. Края двери обкладывают планками из цельного массива дерева. Такой вариант отделки используют для того, чтобы не были заметны внешние границы торцов. Плинтуса и штапики для таких дверей делают цельными. </p>				
				</div>
			</div>
			<div class="group">
				<div class="cell size-50">
					<h3>Разновидности глухих дверей:</h3>
					<div class="group list-blocks type-6">
						<div class="cell size-100">
							<i class="icons-article-arrow-1"></i>
							<p>Ламинированные. </p>
						</div>
						<div class="cell size-100">
							<i class="icons-article-arrow-1"></i>
							<p>Гладкие.</p>
						</div>
						<div class="cell size-100">
							<i class="icons-article-arrow-1"></i>
							<p>Межкомнатные двери, имеющие формованную поверхность.</p>
						</div>
						<div class="cell size-100">
							<i class="icons-article-arrow-1"></i>
							<p>Межкомнатные двери, поверхность которых создана из филенок. </p>
						</div>
					</div>
					<p>У гладких дверей в отличие от остальных разновидностей совершенно гладкая поверхность. </p>
					<p>Ламинированные глухие двери не такие прочные, чем другие аналоги. Именно поэтому их не советуют покупать для установки как входные. Зато в качестве межкомнатных дверей это один из лучших вариантов. Каркас такой двери делают из цельного массива дерева, что позволяет врезать замок с одной стороны, а прикрепить петли с противоположной. Внутри дверь заполняется готовым наполнителем. Благодаря такому подходу к изготовлению  конструкция получается легкой по весу и вместе с тем очень прочной. Такую дверь еще называют облегченной. Глухие двери в офис в Санкт-Петербурге от фабрики офисных перегородок «Монтаж»</p>
					<p>Двери в офисе должны быть износостойкими, надежными, соответствовать выбранной стилистике и обладать хорошей звукоизоляцией. </p>
				</div>
				<div class="cell size-50">
					<img src="/project/images/other/articles/43.jpg">
				</div>
			</div>
			<h3>Как выбрать и купить глухие двери в СПб?</h3>
			<p>Обычно для офисного помещения покупают двери в строгом и выдержанном стиле. Хотите заказать двери глухие в свой офис в Санкт-Петербурге? Обращайтесь к нам! Мы занимаемся изготовлением и установкой дверей в СПб. Стоимость наших изделий демократична, а качество подтверждается долгосрочной гарантией на сами изделия и монтажные работы. </p>
			<p>Зависимо от назначения помещения и требований к его освещенности мы рекомендуем двери глухие или с элементами остекления. </p>
			<p>Межкомнатные офисные двери должны соответствовать выбранному стилю интерьера и положениям пожарной безопасности, а также по причине их частого использования обладать высокой износостойкостью и надежностью.</p>
			<p>Перед тем как приобрести двери глухие, рассмотрим основные критерии, на которые Вы должны обратить внимание при выборе:</p>
			<div class="group list-blocks type-6">
				<div class="cell size-100">
					<i class="icons-article-arrow-1"></i>
					<p>Из всего многообразия нужно определиться с вариантом заполнения дверей – пустотелым или сплошным. В первом варианте дверная конструкция заполняется гофрированным картоном, во втором – ДСП, деревянными рейками или массивом дерева. У каждого из выше представленных видов свои преимущества в плане практичности, веса и стоимости. Купить недорого глухие двери в офис в Санкт-Петербурге, не теряя при этом в добротности и изящности, реально возможно!</p>
				</div>
				<div class="cell size-100">
					<i class="icons-article-arrow-1"></i>
					<p>Глухие двери имеют разные варианты внешнего оформления. Дизайнеры нашей компании разработали модели для различных стилей интерьера – от классики до модерна. На выбор заказчика предоставляется широкий ряд материалов и цветовых решений.</p>
				</div>
			</div>
			<div class="group">
				<div class="cell size-50">
					<img src="/project/images/other/articles/44.jpg">
				</div>
				<div class="cell size-50">
					<p><strong>Основные параметры выбора офисных дверей</strong></p>
					<p>Выбирая межкомнатные двери для офиса, нужно учитывать количество людей, проходящих сквозь них в течение рабочего дня. Если предполагается большой поток посетителей, эксперты рекомендуют остановить выбор на темных оттенках, которые не такие маркие, как светлые, дольше будут сохранять презентабельный внешний вид и не потребуют частого мытья, которое ухудшает качество дверного покрытия. </p>
					<p>Не рекомендуется выбирать самую дешевую дверь в офис, ведь она будет выглядеть несолидно и испортит авторитет компании. Финансовая выгода в этом отношении является обманчивой – такие конструкции не столь прочные и грозят раньше времени выйти из строя, что приведет к дополнительным затратам фирмы на покупку новых дверей. </p>
					<p>Вид дверей следует подбирать с учетом особенностей помещения и типа офиса. Для выдержки строгого официального стиля интерьера в кабинетно-коридорном офисе лучшим вариантом станут глухие двери с ровной гладкой поверхностью. Если помещение обставлено в формате «открытого пространства» с применением офисных перегородок для зонирования вместо глухих дверей рациональнее будет установить стеклянные конструкции. В любом случае фабрика офисных перегородок «Монтаж» в Санкт-Петербурге может предложить Вам изготовление любого из этих видов дверей в кратчайшие сроки. Для офисов комбинированного типа необходимо подбирать подходящие варианты дверных конструкций в каждую зону. </p>
				</div>
			</div>
			<h3>Какие типы офисных дверей мы предлагаем?</h3>
			<p>В деловых помещениях могут монтироваться двери разного типа: ламинированные, шпонированные, комбинированные, стеклянные, одинарные и двойные витражи. Двери из стекла наделяют интерьер легкостью и делают его открытым. Для некоторых офисов актуальной будет установка витражей – приковывающие к себе взгляд богатой палитрой цветов, они отличаются умеренной стоимостью и легко монтируются в любые проемы. </p>
			<p>Солидный вид имеют дверные конструкции, сделанные из массива натурального дерева. В качестве достойной альтернативы дорогостоящим древесным изделиям выступают глухие двери  с ламинированной поверхностью. </p>
			<p>Межкомнатную дверь для офиса любого дизайна и размера можно недорого заказать под ключ в нашей компании. В ассортименте присутствуют модели с утеплением и обивкой, обладающие лучшим показателем звукоизоляции. Замеры производятся бесплатно, на работы по установке дверных конструкций выезжают опытные специалисты. </p>
			<p>Стоимость глухих дверей вместе с их установкой в Санкт-Петербурге зависит от размеров, типа выбранной конструкции, фурнитуры. </p>
			<? include 'inc/gallery.php';?>

		</div>
	</div>
</div>
<? include 'inc/big-red-form.php';?>

<?php get_footer(); // подключаем footer.php ?>