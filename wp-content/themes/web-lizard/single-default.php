<?php
/**
 * Шаблон default
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header();
?>
<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
<div class="page-default">
	<div class="container">
		<div class="two-columns grid">
			<div class="cell-3">
				<? include 'inc/left-menu.php';?>
			</div>
			<div class="cell-9">
				<div class="place">
					<h1><?php the_title(); ?></h1>
					<div class="text">
						<?php if ( have_posts() ) while ( have_posts() ) : the_post(); // старт цикла ?>
						<?
						$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'your_thumb_handle' );
						$thumbnail = $thumbnail['0'];
						if ($thumbnail != "") {?>
						<img src="<?=$thumbnail?>" alt="<?php the_title();?>" class="page-image">
						<?}?>
						<?php the_content(); // контент ?>
						<?php endwhile; // конец цикла ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>