<?php
/**
 * Страница 404 ошибки (404.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // Подключаем header.php ?>
<div class="container">
	<div class="breadcrumbs">
		<ul>
			<li><a href="/">Главная</a></li>
			<li><a href="#">404</a></li>
		</ul>
	</div>
	<!-- / -->
	<div class="content">
		<div class="group content-two-columns">
			<h1>Ошибка 404</h1>
			<p>Такой страницы не существует</p>
		</div>
	</div>
</div>
<!-- // -->

<?php get_footer(); // подключаем footer.php ?>