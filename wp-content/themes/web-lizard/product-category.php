<?php
/**
 * Шаблон обычной страницы (page.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header();


$currentCat = get_category(get_query_var('cat'));
?>

<? include 'inc/index-slider.php';?>
<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>


<div class="page-default product-category">
	<div class="container">
		<div class="two-columns grid">
			<div class="cell-3">
				<? include 'inc/left-menu.php';?>
			</div>
			<div class="cell-9">
				<div class="place">
					<h1><?=$currentCat->name?></h1>
					<div class="product-list">
						<?php if (have_posts()) : while (have_posts()) : the_post(); // если посты есть - запускаем цикл wp ?>
						<?
							
						$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'your_thumb_handle' );
						$thumbnail = $thumbnail['0'];


						if ($thumbnail != "") {

						} else {
							$thumbnail = "/project/images/other/no-photo.jpg";
						}
						?>
						<div class="item">
							<div class="level-0">
								<div class="img" style="background-image: url(<?=$thumbnail?>);"><a href="<? the_permalink() ?>"></a></div>
							</div>
							<div class="level-1">
								<a href="<?php the_permalink() ?>"><?the_title()?></a>
							</div>
							<div class="level-2">
								<p><?php echo strip_tags(get_the_excerpt());?></p>
							</div>
							<div class="level-3">
								<div class="group">
									<div class="cell">Цена:</div>
									<div class="cell"><span>от</span><span class="p"><? the_field("price");?></span><span>руб/м2</span></div>
								</div>
							</div>
							<div class="level-4">
								<div class="group">
									<div class="cell">
										<div class="btn">Заказать расчет</div>
									</div>
									<div class="cell more">
										<a href="<?php the_permalink() ?>">Подробнее</a>
									</div>
								</div>
							</div>
						</div>
						<?php endwhile; // конец цикла
						else: echo '<h2>Нет записей.</h2>'; endif; // если записей нет, напишим "простите" ?>	
					</div>
					<div class="pagination">
						<?pagination();?>
					</div>
					<?
					$seoText = get_field('seo-text', 'category_'.$currentCat->cat_ID);
					if (!empty($seoText)) {?>
						<div class="seo-text">
							<?=$seoText?>
						</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>