<?php
/**
 * Template Name: Контакты
 * @package WordPress
 * @subpackage your-clean-template
 */

			
get_header(); // подключаем header.php ?>
<? include 'inc/index-slider.php';?>
<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
<div class="page-default page-contacts">
	<div class="container">
		<div class="two-columns grid">
			<div class="cell-3">
				<? include 'inc/left-menu.php';?>
			</div>
			<div class="cell-9">
				<div class="place">
					<h1>Контакты</h1>
					<div class="subtitle">Если Вас привлекает комфорт, функциональность, совершенство простых и изысканных форм, то мы ждем Вашего звонка или приходите к нам в офис</div>
					<div class="group">
						<div class="cell size-33 phone">
							<div>+7 (812) 335 20 07</div>
							<div>многоканальный телефон</div>
						</div>
						<div class="cell size-33 point">
							<div>Санкт-Петербург</div>
							<div>ул. Киевская д.5</div>
                                                        <div>БЦ. Энерго, 1 этаж</div>
						</div>
						<div class="cell size-33 email">
							<div>zakaz@fmontaj.ru</div>
							<div>электронный адрес компании</div>
						</div>
					</div>
					<h2>Как проехать</h2>
										<div class="map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2000.8341883460987!2d30.324210524033738!3d59.90170200614005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcb6d6ea3c5c8f7b1!2z0K3QndCV0KDQk9CeINC00LXQu9C-0LLQvtC5INGG0LXQvdGC0YA!5e0!3m2!1sru!2sru!4v1484833673066" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? include 'inc/big-red-form.php';?>

<?php get_footer(); // подключаем footer.php ?>