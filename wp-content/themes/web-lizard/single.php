<?php
/**
 * Шаблон отдельной записи (single.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php 

$page_template = get_field("template");
if (!empty($page_template) and $page_template != "none") {
	include $page_template; 
} else {
	$page_type = get_field("type");
	switch ($page_type) {
		case '2':
			include 'single-product.php'; 
			break;
		default:
			include 'single-default.php'; 
			break;
	}
}
?>
<?php get_footer(); // подключаем footer.php ?>