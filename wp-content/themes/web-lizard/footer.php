<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
?>

	<div class="footer">
		<div class="level-0">	
			<div class="container">
				<div class="grid">
					<div class="cell-3">
						<a href="/"><i class="icons-footer-logo"></i></a>
					</div>
					<div class="cell-9">
						<div class="row phone">
							<div>+7 (812) 335 20 07</div>
							<div>многоканальный телефон</div>
						</div>
						<div class="row address">
							<div>Санкт-Петербург</div>
							<div>ул. Киевская д.5</div>
                                                        <div>БЦ. Энерго, 1 этаж</div>
						</div>
						<div class="row">
							<div class="btn get-callback">Заказать обратный звонок</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="level-1">
			<div class="container">
				<div class="row">
					<div class="title">
						Компания
					</div>
					<ul>
						<li><a href="/">Главная</a></li>
						<li><a href="/about/">О компании</a></li>
						<li><a href="/main-gallery/">Фотогалерея</a></li>						
					</ul>
					<ul>
						<li><a href="/price/">Цены</a></li>
						<li><a href="/complex/">Комплексные решения</a></li>
						<li><a href="/contacts/">Контакты</a></li>
					</ul>
				</div>
				<div class="row">
					<div class="title">
						продукция
					</div>
					<ul>
						<li><a href="/ofisnye-peregorodki/">Офисные перегородки</a></li>
						<li><a href="/stojki-resepshn/">Стойки ресепшн</a></li>
						<li><a href="/peregorodki-dlya-torgovyh-tsentrov/">Перегородки для торговых центров</a></li>
						<li><a href="/roletty/">Роллеты</a></li>
					</ul>
					<ul>
						<li><a href="/transformiruemye-peregorodki/">Трансформируемые перегородки</a></li>
						<li><a href="/demontazh-i-perenos-peregorodok/">Демонтаж и перенос перегородок</a></li>
						<li><a href="/dveri-na-zakaz/">Двери на заказ</a></li>
						<li><a href="/konstruktsii-dlya-chastnyh-intererov/">Конструкции для частных интерьеров</a></li>
					</ul>
					<ul>
						<li><a href="/kozyrki-iz-stekla/">Козырьки из стекла</a></li>
						<li><a href="/ostekleniya-dlya-fasadov-zdanij/">Остекления для фасадов зданий</a></li>
						<li><a href="/ograzhdeniya-lestnits/">Ограждения лестниц</a></li>
						<li><a href="/konstruktsii-iz-stekla-i-metalla/">Конструкции из стекла и металла</a></li>
					</ul>
				</div>
				<div class="row">
					<div class="title">Мы на связи 24/7</div>
					<div class="form">
						<form id="footer-form">
							<div>
								<input type="text" placeholder="Ваше имя" value="" name="name">
							</div>
							<div>
								<input type="text" placeholder="Ваш телефон" value="" name="phone">
							</div>
							<div>
								<div class="btn">Перезвоните мне!</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="level-2">
			<div class="container">
				© “Монтаж”. Все права защищены
			</div>
		</div>
	</div>
</div>

<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
</body>
</html>