<div id="callback-form-place" class="modal-window">
	<i class="icons-close"></i>
	<div class="title">
		<div>Мы перезвоним вам</div>
		<div>В течении 15 минут</div>
	</div>
	<form id="modal-callback-form" novalidate="novalidate">
		<input type="hidden" name="form-title" value="Заказать обратный звонок" id="form-title">
		<div class="field-name">
			<input type="text" placeholder="Ваше имя" name="name">
		</div>
		<div class="field-phone">
			<input type="text" placeholder="Ваш телефон" class="phone" name="phone">
		</div>		
		<div class="field-comment">
			<textarea name="comment" placeholder="Комментарий"></textarea>
		</div>
		<div class="submit">
			<div class="btn">Отправить</div>
		</div>
		<input type="hidden" name="form-url" value="<?currentUrl()?>">
	</form>
</div>

