<?
$gallery = get_field('gallery');
if ($gallery) {
?>
<div class="cat-slider">
	<div class="big-img" style="background-image: url(<?=$gallery[0]['sizes']['large']?>);">
	</div>
	<div class="slider">
		<? 
		foreach ($gallery as $item) {?>
		<div class="item" style="background-image: url(<?=$item['sizes']['medium']?>);" data-big="<?=$item['sizes']['large']?>" data-title="<?=$item['title']?>"></div>
		<?}?>
	</div>
</div>
<?}?>