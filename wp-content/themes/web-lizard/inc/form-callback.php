			<!-- / -->
			<div class="part-4 main-callback-form">
				<div class="container">
					<h2>
						<? if (lang == "ru") {
							echo "Укажите номер телефона";
						} else {
							echo "Enter phone number";
						}?>
					</h2>
					<h3>
						<? if (lang == "ru") {
							echo "И мы вам перезвоним";
						} else {
							echo "And we will call you back";
						}?>
					</h3>
					<form action="" id="main-callback-form">
						<div class="group">
							<div class="cell size-50">
								<div class="input-place input-phone">
									<input type="text" class="phone" name="phone" placeholder="<? if (lang == "ru") { echo "Контактный телефон";} else { echo "Phone";}?>">
								</div>							
							</div>
							<div class="cell size-50">
								<div class="btn green"><? if (lang == "ru") { echo "Перезвоните мне";} else { echo "Call me back";}?></div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- / -->
