<?php
/**
 * Шаблон обычной страницы (page.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header();


$currentCat = get_category(get_query_var('cat'));
?>

<? include 'inc/index-slider.php';?>
<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>

<div class="page-default product-page">
	<div class="container">
		<div class="two-columns grid">
			<div class="cell-3">
				<? include 'inc/left-menu.php';?>
			</div>
			<div class="cell-9">
				<div class="place">
					<h1><?php the_title(); ?></h1>
					<div class="product">
						<div class="gallery">
							<?
							$gallery = get_field('gallery');
							?>
							<div class="product-slider">
								<div class="big-img" style="background-image: url(<?=$gallery[0]['sizes']['large']?>);">
								</div>
								<div class="slider">
									<? foreach ($gallery as $item) {?>
									<div class="item" style="background-image: url(<?=$item['sizes']['medium']?>);" data-big="<?=$item['sizes']['large']?>" data-title="<?=$item['title']?>"></div>
									<?}?>
								</div>
							</div>
						</div>
						<div class="info">
							<div class="price-order">
								<div class="price">
									<spna>от</span>
									<span class="p"><?the_field("price");?></span>
									<span>руб/м2</span>
								</div>
								<div class="order">
									<div class="btn"><span>Заказать расчет</span></div>
								</div>
							</div>
							<div class="description">
								<?
								echo $post->post_content;
								?>
							</div>
						</div>
					</div>
					<div class="characteristics">
						<? the_field("characteristics");?>
					</div>
					<div class="form-call">
						<form id="call-form">
							<h3>Вызвать замерщика бесплатно</h3>
							<p>в любое удобное Вам время</p>
							<div class="group">
								<div class="cell size-33">
									<input type="text" placeholder="Ваше имя">
								</div>
								<div class="cell size-33">
									<input type="text" placeholder="Ваш телефон">
								</div>
								<div class="cell size-33">
									<div class="btn">Вызвать замерщика</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>