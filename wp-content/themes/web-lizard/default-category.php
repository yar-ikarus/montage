<?php
/**
 * Шаблон обычной страницы (page.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header();

$currentCat = get_category(get_query_var('cat'));
?>

<? include 'inc/index-slider.php';?>
<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>


<div class="page-default page-category">
	<div class="container">
		<div class="two-columns grid">
			<div class="cell-3">
				<? include 'inc/left-menu.php';?>
			</div>
			<div class="cell-9">
				<div class="place">
					<h1><?=$currentCat->name?></h1>
					<?
					$gallery = get_field('gallery', 'category_'.$currentCat->cat_ID);
					if ($gallery) {
					?>
					<div class="cat-slider">
						<div class="big-img" style="background-image: url(<?=$gallery[0]['sizes']['large']?>);">
						</div>
						<div class="slider">
							<? 
							foreach ($gallery as $item) {?>
							<div class="item" style="background-image: url(<?=$item['sizes']['medium']?>);" data-big="<?=$item['sizes']['large']?>" data-title="<?=$item['title']?>"></div>
							<?}?>
						</div>
					</div>
					<?} else {
						echo "Раздел пуст.";
					}?>

					<?
					$args = array(
						"category" => $currentCat->cat_ID
					);
					$post_list = get_posts($args);

					if (!empty($post_list)) {?>
						<div class="post-list">
							<?
							foreach ($post_list as $post) {
								$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
								$thumbnail = $thumbnail['0'];
							?>
							<div class="item" style="background-image: url(<?=$thumbnail?>)">
								<a href="<?the_permalink($post->ID)?>"><div><?=$post->post_title?></div></a>
							</div>
							<?}?>
						</div>
					<?}?>

					<?
					$seoText = get_field('seo-text', 'category_'.$currentCat->cat_ID);
					if (!empty($seoText)) {?>
						<div class="seo-text">
							<?=$seoText?>
						</div>
					<?}?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); // подключаем footer.php ?>