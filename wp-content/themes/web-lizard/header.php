<?php
/**
 * Шаблон шапки (header.php)
 * @package WordPress
 * @subpackage your-clean-template
 */


?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); // кодировка ?>">
	<?php /* RSS и всякое */ ?>
	<link rel="alternate" type="application/rdf+xml" title="RDF mapping" href="<?php bloginfo('rdf_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php bloginfo('rss_url'); ?>">
	<link rel="alternate" type="application/rss+xml" title="Comments RSS" href="<?php bloginfo('comments_rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); // абсолютный путь до темы ?>/style.css">
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<link rel="stylesheet" href="/project/reject/reject.css" media="all" />
		<script type="text/javascript" src="/project/reject/reject.min.js"></script>
	<![endif]-->
	<title><?php typical_title();  ?></title>
	
	<?php wp_head(); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,400i,500,700&amp;subset=cyrillic-ext" rel="stylesheet">

	<link rel="stylesheet" href="/project/css/style.min.css?<?echo time();?>">

	<link rel="stylesheet" href="/project/js/lib/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="/project/js/lib/owl-carousel/owl.transitions.css">

	<script src="/project/js/lib/jquery-1.12.4.min.js"></script>

	<script src="/project/js/lib/owl-carousel/owl.carousel.min.js"></script>

	<script src="/project/js/lib/ui/jquery-ui.min.js"></script>
	<script src="/project/js/lib/icheck.min.js"></script>
	<script type="text/javascript" src="/project/js/lib/fancybox/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="/project/js/lib/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
	
	<script src="/project/js/lib/jquery.validate.min.js"></script>
	<script src="/project/js/lib/jquery.maskedinput.min.js"></script>

	<script src="/project/js/fvalid.js"></script>
	<script src="/project/js/forms.js"></script>
	<script src="/project/js/main.js?<?echo time();?>"></script>
</head>
<body>
	<div class="darkness"></div>
	<div class="grid-mask">
		<div class="container">
			<div class="grid">
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
				<div class="cell-1"></div>
			</div>
		</div>
	</div>
	<div class="fixed-adaptive-block">
		<div class="left">
			<div class="sandwich">
				<i class="icons-sandwich"></i>
			</div>
		</div>
		<div class="right">
			<a href=""><i class="icons-header-adapt-phone"></i></a>
			<a href=""><i class="icons-header-adapt-point"></i></a>
			<span class="btn get-zamer">Вызвать замерщика</span>
		</div>
	</div>

	<div id="zamer-form-place" class="modal-window">
		<i class="icons-close"></i>
		<div class="title-form">
			<div>Бесплатный вызов замерщика</div>
		</div>
		<p>перезвоним Вам в течении 15 минут</p>
		<div class="form-place">
			<form id="zamer-callback-form" novalidate="novalidate">
				<input type="hidden" name="form-title" value="Обратный звонок">
				<div class="field-name">
					<input type="text" placeholder="Ваше имя" name="name">
				</div>
				<div class="field-phone">
					<input type="text" placeholder="Ваш телефон" name="phone">
				</div> 
				<div class="submit">
					<div class="btn red">Отправить</div>
				</div>
			</form>
		</div>
	</div>

	<div id="callback-form-place" class="modal-window">
		<i class="icons-close"></i>
		<div class="title-form">
			<div>Заказать обратный звонок</div>
		</div>
		<p>перезвоним Вам в течении 15 минут</p>
		<div class="form-place">
			<form id="modal-callback-form" novalidate="novalidate">
				<input type="hidden" name="form-title" value="Обратный звонок">
				<div class="field-name">
					<input type="text" placeholder="Ваше имя" name="name">
				</div>
				<div class="field-phone">
					<input type="text" placeholder="Ваш телефон" name="phone">
				</div> 
				<div class="submit">
					<div class="btn red">Отправить</div>
				</div>
			</form>
		</div>
	</div>


	<div class="sandwich-menue">	
		<ul>
			<li class="active"><a href="/">Главная</a></li>
			<li><a href="/about">О компании</a></li>
			<li><a href="/main-gallery">Фотогалерея</a></li>
			<li><a href="/price">Цены</a></li>
			<li><a href="/ofisnye-peregorodki/">Продукция</a></li>
			<li><a href="/complex">Комплексные решения</a></li>
			<li><a href="/contacts">Контакты</a></li>
		</ul>
	</div>
	<div class="wrapper">
		<div class="adapt-header">
			<a href="/"><i class="icons-logo-adapt"></i></a>
		</div>
		<div class="header">
			<div class="part-0">
				<div class="container">
					<div class="grid">
						<div class="cell-3">
							<a href="/"><img src="/project/images/icons/main-logo.png"></a>
						</div>
						<div class="cell-9">
							<div class="level-0">
								<div class="row">
									<p>Инжинеринг и монтаж конструкций из стекла и металла</p>
								</div>
								<div class="row">
									<div class="btn get-zamer">
										<i class="icons-btn-zamer"></i>
										<span>Вызвать замерщика</span>
									</div>
								</div>
							</div>
							<div class="level-1">
								<div class="row phone">
									<div>+7 (812) 335 20 07</div>
									<div>многоканальный телефон</div>
								</div>
								<div class="row address">
									<div>Санкт-Петербург</div>
					                <div>ул. Киевская д.5</div>
                                    <div>БЦ. Энерго, 1 этаж</div>
								</div>
								<div class="row">
									<div class="btn get-callback">Заказать обратный звонок</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="part-1">
				<div class="container">
					<div class="menu">
						<ul>
							<li class="<?if (is_front_page()) {echo "active";}?>"><a href="/">Главная</a></li>
							<li class="<?activeMenu("about");?>"><a href="/about">О компании</a></li>
							<li class="<?activeMenu("main-gallery");?>"><a href="/main-gallery">Фотогалерея</a></li>
							<li class="<?activeMenu("price");?>"><a href="/price">Цены</a></li>
							<li class="production <?activeMenu("production");?>">
								<a href="#">Продукция</a>
								<div class="dropdown">
									<div class="group">
										<div class="cell" style="background-image: url(/project/images/other/menu/production-1.jpg);">
											<a href="/ofisnye-peregorodki/">
												<div><span>Офисные перегородки</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-2.jpg);">
											<a href="/stojki-resepshn/">
												<div><span>Стойки ресепшн</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-3.jpg);">
											<a href="/peregorodki-dlya-torgovyh-tsentrov/">
												<div><span>Перегородки для торговых центров</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-4.jpg);">
											<a href="/roletty/">
												<div><span>Роллеты</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-5.jpg);">
											<a href="/transformiruemye-peregorodki/">
												<div><span>Трансформируемые перегородки</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-6.jpg);">
											<a href="/dveri-na-zakaz/">
												<div><span>Двери на заказ</span></div>
											</a>
										</div>
										<!-- козырьки -->
										<div class="cell" style="background-image: url(/project/images/other/menu/production-10.jpg);">
											<a href="/kozyrki-iz-stekla/">
												<div><span>Козырьки из стекла</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-7.jpg);">
											<a href="/konstruktsii-dlya-chastnyh-intererov/">
												<div><span>Конструкции для частных интерьеров</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-8.jpg);">
											<a href="/ograzhdeniya-lestnits/">
												<div><span>Ограждения лестниц</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-9.jpg);">
											<a href="/konstruktsii-iz-stekla-i-metalla/">
												<div><span>Конструкции из стекла и металла</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-11.jpg);">
											<a href="/ostekleniya-dlya-fasadov-zdanij/">
												<div><span>Остекления для фасадов зданий</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-12.jpg);">
											<a href="/demontazh-i-perenos-peregorodok/">
												<div><span>Демонтаж и перенос перегородок</span></div>
											</a>
										</div>
										<!-- // -->
										<div class="cell" style="background-image: url(/project/images/other/menu/production-13.jpg);">
											<a href="?">
												<div><span>Монтаж перегородок из материалов заказчика</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-14.jpg);">
											<a href="?">
												<div><span>Сантехнические перегородки</span></div>
											</a>
										</div>
										<div class="cell" style="background-image: url(/project/images/other/menu/production-15.jpg);">
											<a href="?">
												<div><span>Цены</span></div>
											</a>
										</div>
									</div>
								</div>
							</li>
							<li class="<?activeMenu("complex");?>"><a href="/complex">Комплексные решения</a></li>
							<li class="<?activeMenu("contacts");?>"><a href="/contacts">Контакты</a></li>
						</ul>
					</div>
					<div class="search">
						<form action="/">
							<input type="text" placeholder="Что Вы ищите?" name="s">
						</form>
					</div>
				</div>
			</div>
		</div>
	