<?php
/**
 * Шаблон поиска (search.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 

<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?>
<div class="page-default page-search">
	<div class="container">
		<div class="two-columns grid">
			<div class="cell-3">
				<? include 'inc/left-menu.php';?>
			</div>
			<div class="cell-9">
				<div class="place">
					<h1><?php printf('Поиск по строке: %s', get_search_query());  ?></h1>
					<div class="loop">
					<?php if (have_posts()) : while (have_posts()) : the_post();?>
						<div class="item">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
							<?php the_content('');?>
						</div>
					<?php endwhile; // конец цикла
					else: echo '<h2>Нет записей.</h2>'; endif; ?>	 
					</div>
					<?php pagination();  ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer();  ?>


