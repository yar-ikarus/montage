<?php
/**
 * Шаблон рубрики (category.php)
 * @package WordPress
 * @subpackage your-clean-template
 */
get_header(); // подключаем header.php ?> 

<?
$currentCat = get_category(get_query_var('cat'));


switch ($currentCat->slug) {
	case 'ofisnye-peregorodki':
		include 'product-category.php'; 
		break;
	case 'main-gallery':
		include 'gallery.php'; 
		break;
	default:
		include 'default-category.php'; 
		break;
}

?>

<?php get_footer(); // подключаем footer.php ?>