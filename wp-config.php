<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'montage');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1TAE+DnKt!7deA]WV&d/|RhRY&k%=0S2hL$d^z inX |qOa,^Akv$SU{!$:1IYs=');
define('SECURE_AUTH_KEY',  '|.KAzH)p?7eTA#E4MP<>IV^w<umhaXK,1] oQ6?4Cg=!>ekN+[Gs:V0w_lV>St-?');
define('LOGGED_IN_KEY',    'r]sXuh 1ZeYudG Tm4K~Xi}yg]e:j`_61XpOx`Yd!b8,Pc3Lu9FQx0{$iN7W2:iD');
define('NONCE_KEY',        'f&hZa,G!2hX+1vue|bG)<d2r&,^_]Bos*Pdo)l8PbZ9j0{LozANs0k-xUC(ryh;R');
define('AUTH_SALT',        '7DZH:VWvV/{!;XgBs~MzXc*Doyp@hMl$>BkrHr$*nx$g;`clCH1BmG]Cf-a^:=X(');
define('SECURE_AUTH_SALT', 'HeOy-g;2FDf-I6G[JMx7g7cpxY]?KY.[FAE^e6V(ih.2>VR7JdudNmb7hG}mG$$R');
define('LOGGED_IN_SALT',   'LzW!Gwe]%P,>I1!^RKi?axS^hG8&lfdom}!pQ?gK^gn}`?117)0/gmv@WT`cy:Uk');
define('NONCE_SALT',       ']i~P;u5 3+=YA}f3c{Av)V-/_ii]9).Hv;lrxt?6O:oWZ[njeJ[uq@2zy8rvHnB4');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
